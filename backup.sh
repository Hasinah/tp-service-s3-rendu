#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset
# On definit une variable vide qui contiendra le dossier que l'on souhaite archiver
DIR=""

# Si on a au moins un argument, on le mets dans la variable DIR
if [ $# -gt 0 ]; then
    DIR="$1"
fi

# Tant que la variable DIR est vide ou ne contient pas un dossier, on demande a l'utilisateur d'entrer un dossier.
while [ -z $DIR ] || [ ! -d $DIR ]
do
    echo -n "Entrez un dossier "
    read DIR
done

# On cree un format pour le nom de l'archive et on cree l'archive
date=$(date '+%d-%m-%Y-%H-%M-%S')
tar cfz Hasinah-backup-$date.tar.gz $DIR/*

echo "Backup a été créé"
mc config host add minio-hasinah $URL_MINIO $MINIO_ACCESS $MINIO_KEY
mc cp Hasinah-backup-$date.tar.gz minio-hasinah/backup
