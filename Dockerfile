FROM golang:1.13-alpine
COPY . .
COPY ./backup.sh /
RUN chmod +x /backup.sh
CMD ["backup.sh"]
